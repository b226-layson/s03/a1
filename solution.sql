/* 

S03: MySQL CRUD Operations (Activity)

1. Create a new s03-a1 folder and then create a file named solution.sql and write the SQL code necessary to insert the following records in the tables and to perform the following query.
2. Add the following records to the blog_db database:
3. Get all the post with an Author ID of 1.

 */


 USE blog_db;

 /* 
 
johnsmith@gmail.com
passwordA
2021-01-01 10000

juandelacruz@gmail.com
passwordB
2021-01-01 20000

janesmith@gmail.com
passwordC
2021-01-01 30000

mariadelacruz@gmail.com
passwordD
2021-01-01 40000

johndoe@gmail.com
passwordE
2021-01-01 50000


  */

DESCRIBE users;

/* 
1
First Code
Hello World!
2021-01-02 10000

1
Second Code
Hello Earth!
2021-01-02 20000

2
Third Code
Welcome to Mars!
2021-01-02 30000

4
Fourth Code
Bye bye solar system!
2021-01-02 40000
 */

DESCRIBE posts;

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Hello to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

SELECT * FROM users;
-- checked
SELECT * FROM posts;
-- checked


/* 
3. Get all the post with an Author ID of 1.
 */
SELECT * FROM posts WHERE author_id = "1";
/* 
MariaDB [blog_db]> SELECT * FROM posts WHERE author_id = "1";
+----+-----------+-------------+--------------+---------------------+
| id | author_id | title       | content      | datetime_posted     |
+----+-----------+-------------+--------------+---------------------+
|  1 |         1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth! | 2021-01-02 02:00:00 |
+----+-----------+-------------+--------------+---------------------+
2 rows in set (0.003 sec)
 */


 /*  
 4. Get all the user's email and datetime of creation.

 */
SELECT email, datetime_created FROM users;
/* 
MariaDB [blog_db]> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+
5 rows in set (0.000 sec)
 */

 /* 
 
5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
  */
SELECT * FROM posts;
/* 
MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_posted     |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Hello to Mars!        | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+
4 rows in set (0.000 sec)
 */
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";
/* 
MariaDB [blog_db]> UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";
Query OK, 1 row affected (0.005 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  1 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Hello to Mars!                    | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+
4 rows in set (0.001 sec)
 */

  /* 
  
6. Delete the user with an email of "johndoe@gmail.com".
   */
   SELECT * FROM users;
/* 
MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+
5 rows in set (0.000 sec)
 */
DELETE FROM users where email  = "johndoe@gmail.com";
 /* 
 MariaDB [blog_db]> DELETE FROM users where email  = "johndoe@gmail.com";
Query OK, 1 row affected (0.006 sec)

MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.002 sec)
  */